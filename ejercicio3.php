<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript">
            window.addEventListener("load",arranca);
            function arranca(){
                document.querySelector("line").addEventListener("click",salta);
            }
            function salta(event){
                location.reload();
            }
        </script>
    </head>
    <body>
        <?php
        $longitud=rand(1,100);
        
        print "<p>Longitud: $longitud</p>";
        echo "<br>";
        ?>
        <svg width="1000px" height="10px">
        <line x1="1" y1="5" x2="<?=$longitud?>" y2="5" stroke="red" stroke-width="10"/>
        </svg>
    </body>
</html>