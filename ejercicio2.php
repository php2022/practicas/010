<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $linea=rand(1,100);
        
        print "<p>Longitud: $linea</p>";
        echo "<br>";
        ?>
        <svg width="<?=$linea?>px" height="10px">
        <?php
        //forma alternativa de imprimir
        echo '<line x1="1" y1="5" x2="' . $linea . '" y2="5" stroke="red" stroke-width="10">';
        
        /**
         * Los IDES trabajaran mucho mejor si intentas escribir HTML fuera de PHP
         */
        ?>
        </svg>
    </body>
</html>
